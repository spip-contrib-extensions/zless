**Ce plugin n'est plus maintenu, il est vivement conseillé de migrer vers la famille Spipr et scss**


----

Un plugin pour utiliser Zpip-dist + LESS (fournit par le plugin less-css)

Le plugin contient une surcharge less des CSS de Zpip-dist,
et modifie la balise `#CSS` utilisee par Zpip-dist pour pouvoir prendre indiferrement
une feuille css ou une feuille less :
lorsque `#CSS{css/typo.css}` est rencontre, on cherche dans le chemin de SPIP la feuille `typo.css` ou `typo.less`
La plus prioritaire des deux est retenue.

Si il s'agit de `typo.less`, elle est compilee avec `less-css`
On peut donc surcharger les CSS (au sens de SPIP) indiferrement au format `.less` ou `.css`

Par ailleurs, toutes les feuilles sont configurees ici par `css/config.less`
qui fournit les parametres de typo de base
